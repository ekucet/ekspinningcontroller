//
//  ViewController.swift
//  EKSpinnigIndicator
//
//  Created by Erkam on 27/07/15.
//  Copyright (c) 2015 Erkam KÜCET. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var ekSpinnigView: SpinningView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ekSpinnigView.alpha = 0.0
        
        self.view.backgroundColor = UIColor.darkGrayColor()
        self.ekSpinnigView.tintColor = UIColor.blackColor()
        self.ekSpinnigView.backgroundColor = self.view.backgroundColor
        
        UIView.animateWithDuration(3.0, animations: { () -> Void in
            
            self.ekSpinnigView.alpha = 1.0
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

